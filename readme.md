# JUTG ITB

## DEFINICIÓ DEL PROYECTE
En este proyecto podrás poner a prueba tus habilidades de programación. El programa te presentará un problema a través de la consola, junto con una serie de conjuntos de pruebas que contienen tanto la entrada (input) como la salida (output). Estos conjuntos de pruebas sirven principalmente como ejemplos de solución del problema, y te ayudarán a comprender el problema y a encontrar la solución.

Tu tarea consistirá en crear un programa capaz de resolver el ejercicio y mostrar el resultado a través de la consola. Si logras resolver el ejercicio correctamente, podrás avanzar al siguiente. En caso de que no obtengas el resultado esperado, deberás seguir intentando hasta lograr resolver correctamente el problema.

Y el proyecto comienza con una historia para poder meter en contexto al usuario y que tenga una mejor interación con el juego.

### Resumen historia
Un programador joven y ambicioso se ofrece a solucionar el problema del ordenador poseído en una oficina, pero se le advierte que el virus es muy peligroso y que si no lo resuelve correctamente, estará condenado a vivir acompañado del virus por el resto de su vida. El programador acepta el desafío y se enfrenta a una serie de ejercicios propuestos por el ordenador poseído. Sin embargo, las cosas se complican cuando descubre que el ordenador no es un simple virus, sino una inteligencia artificial malévola que ha estado esperando a alguien lo suficientemente brillante como él para liberarse.

## SISTEMA DE DATOS
El proyecto se compone de una clase que se utiliza para resolver los problemas. Cada problema consta de un enunciado, una serie de entradas y salidas, tanto públicas como privadas, un contador de intentos y una lista con todos los intentos realizados hasta el momento. Además, la clase cuenta con tres funciones que se utilizan para trabajar con los problemas:
- imprProblema esta función muestra por pantalla el enunciado y los conjuntos de pruebas del problema.
- resolverProblema: se encarga de recoger la respuesta del usuario y comprobar si es correcta o no. En caso de que la respuesta sea correcta, se pasará al siguiente problema. En caso contrario, el usuario tendrá la oportunidad de intentarlo de nuevo.
- imprEstat: esta función muestra por pantalla el estado actual del problema una vez que ha sido completado (número de intentos, lista de intentos y si ha sido completado correctamente o no).