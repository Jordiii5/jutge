import com.google.gson.Gson
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import kotlin.system.exitProcess


var usuarioAlumno = ""
class Profesor(val nombre: String, var correo: String, var contraseña: String) {
    fun crearCuenta() {
        // 1. Leer el contenido del archivo JSON en un objeto de lista de objetos Profesor
        val gson = Gson()
        val archivo = File("./src/main/kotlin/Profesor.json")
        val contenido = archivo.readText()
        val listaProfesor = try {
            gson.fromJson(contenido, Array<Profesor>::class.java).toMutableList()
        } catch (ex: Exception) {
            mutableListOf<Profesor>()
        }

        // 2. Pedir al usuario que introduzca su nombre, correo electrónico y contraseña
        print("${white}Introduce tu nombre completo: ")
        val nombre = readLine()?.trim() ?: ""

        print("Introduce tu correo electrónico: ")
        val correo = readLine()?.trim() ?: ""

        print("Introduce tu contraseña: ")
        val contraseña = readLine()?.trim() ?: ""

        // 3. Verificar si el correo electrónico ya existe en la lista de profesores
        if (listaProfesor.any { it.correo == correo }) {
            println("${redBold}Este correo ya pertenece a un usuario, por favor inicia sesión.$reset")
            println("${white}Dirigido a la pantalla de iniciar sesión.$reset")
            iniciarSesion()
            return
        }

        // 4. Crear un nuevo objeto Profesor con los datos introducidos por el usuario
        val nuevoProfesor = Profesor(nombre, correo, contraseña)

        // 5. Agregar el nuevo objeto Profesor a la lista de profesores
        listaProfesor.add(nuevoProfesor)

        // 6. Escribir el contenido actualizado de la lista de profesores de vuelta al archivo JSON
        val contenidoActualizado = gson.toJson(listaProfesor)
        archivo.writeText(contenidoActualizado)

        // 7. Imprimir el correo electrónico del nuevo profesor agregado
        println("${green}El nuevo profesor agregado, llamado ${nuevoProfesor.nombre} con el correo electrónico: ${nuevoProfesor.correo}$reset")
    }

    fun iniciarSesion() {
        // 1. Leer el contenido del archivo JSON en un objeto de lista de objetos Profesor
        val gson = Gson()
        val archivo = File("./src/main/kotlin/Profesor.json")
        val contenido = archivo.readText()
        val listaProfesor = gson.fromJson(contenido, Array<Profesor>::class.java).toList()

        // 2. Pedir al usuario que introduzca su correo electrónico y contraseña
        print("${white}Introduce tu correo electrónico: ")
        val correoBusqueda = readLine()?.trim() ?: ""

        print("Introduce tu contraseña: ")
        val contraseñaBusqueda = readLine()?.trim() ?: ""

        // 3. Buscar en la lista de objetos Profesor si existe algún objeto que contenga el correo electrónico y la contraseña introducidos por el usuario
        val profesorCoincidente = listaProfesor.find { profesor -> profesor.correo == correoBusqueda && profesor.contraseña == contraseñaBusqueda }

        // 4. Si existe un objeto Profesor con el correo electrónico y la contraseña introducidos por el usuario, saludar al usuario
        if (profesorCoincidente != null) {
            println("Hola ${profesorCoincidente.nombre.uppercase()}!")
            menu()
        } else {
            println("${redBold}No se encontró ningún profesor con el correo electrónico y la contraseña introducidos.$reset")
            iniciarSesion()
        }
    }

    fun menu() {
        println("$bold${white}")
        println("MENU:".uppercase())
        println("     1- Añadir nuevos problemes ".uppercase())
        println("     2- Obtener un informe del trabajo del alumno ".uppercase())
        println("     3- Cambiar de cuenta ".uppercase())
        println("     4- Salir ".uppercase())
        println(reset)
        do {
            print("${mgreen}Elige una opción válida: $reset".uppercase())
            var opcioUsuari = scanner.next()
            when (opcioUsuari) {
                "1" -> {
                    println()
                    afegirProblema()
                    println()
                    menu()
                }

                "2" -> {
                    println()
                    println("${white}Introduce el mail del alumno que quieres reportar:$reset")
                    var correoAlumno: String = scanner.next()
                    usuarioAlumno += correoAlumno
                    reportAlumne()
                    println()
                    menu()
                }
                "3"->{
                    println()
                    main()
                }
                "4" -> {
                    println()
                    println("${white}Hasta luego!$reset")
                    exitProcess(0)
                }

                else -> opcioUsuari = "error"
            }
        } while (opcioUsuari == "error")
    }

    fun afegirProblema() {
        val atributsProblemaNom =
            listOf("tema", "titulo", "enunciado", "input publico", "output publico", "input privado", "output privado")
        val atributsProblemaValor = mutableListOf<String>()
        for (atribut in atributsProblemaNom) {
            print("${white}Introduce $atribut: $reset".uppercase())
            val inputUsuari = scanner.next() + scanner.nextLine()
            atributsProblemaValor.add(inputUsuari)
        }
        val problema = Problema(
            0,
            atributsProblemaValor[0],
            atributsProblemaValor[1],
            atributsProblemaValor[2],
            atributsProblemaValor[3],
            atributsProblemaValor[4],
            atributsProblemaValor[5],
            atributsProblemaValor[6]
        )
        Database().anadirProblema(problema)
    }

    fun reportAlumne() {
        val archivo = File("src/main/kotlin/historial/historial_${usuarioAlumno}.json")
        val contenido = archivo.readLines()
        println("${white}Datos a reportar:".uppercase())
        println("     1- Sacar una puntuación en función de los problemas resueltos".uppercase())
        println("     2- Descontar por intentos".uppercase())
        println("     3- Mostrarlo de manera gràfica".uppercase())
        println("     4- Mostral el historial del alumno".uppercase())
        println("     5- Volver al menu$reset".uppercase())
        do {
            print("${mgreen}Elige una opción válida: $reset".uppercase())
            var opcioUsuari = scanner.next()
            when (opcioUsuari) {
                "1" -> {
                    println()
                    var contadorProblemas = 0
                    for (line in contenido) {
                        contadorProblemas++
                    }
                    println("${green}Puntuación total = ${(contadorProblemas * 10) / 20}/10$reset".uppercase())
                    println()
                    reportAlumne()
                }

                "2" -> {
                    println()
                    for (line in contenido) {
                        val problema = Json.decodeFromString<Problema>(line)
                        print("${white}Título: ${problema.titulo}  ||".uppercase())
                        if (problema.intentos == 1) {
                            println("\t${green}Puntuación = ${10}$reset".uppercase())
                        } else {
                            println("\t${green}Puntuación = ${10 - problema.intentos}$reset".uppercase())
                        }
                    }
                    println()
                    reportAlumne()
                }

                "3" -> {
                    println()
                    for (line in contenido) {
                        val problema = Json.decodeFromString<Problema>(line)
                        print("${white}Título: ${problema.titulo}  ||  $reset".uppercase())
                        if (problema.intentos == 1) {
                            repeat(10) {
                                print("${green}✸$reset")
                            }
                        } else {
                            repeat(10 - problema.intentos) {
                                print("${green}✸$reset")
                            }
                        }
                        println()
                    }
                    println()
                    reportAlumne()
                }

                "4" -> {
                    println()
                    for (line in contenido) {
                        val problema = Json.decodeFromString<Problema>(line)
                        println("${box} Título: ${problema.titulo} ")
                        println(" NºIntentos: ${problema.intentos} ")
                        println(" Intentos: ${problema.intentos_usuario} ")
                        println(" Resuelto: ${problema.resuelto}\n $reset")
                    }
                    println()
                    menu()
                }

                "5" -> {
                    println()
                    menu()
                }

                else -> opcioUsuari = "error"
            }
        } while (opcioUsuari == "error")
    }
}
