import kotlinx.serialization.Serializable

@Serializable
class Problema(
    val id_problema: Int,
    val tema:String,
    val titulo:String,
    val enunciado:String,
    val input_public:String,
    val output_public:String,
    val input_privado:String,
    val output_privado:String,
){
    var resuelto:Boolean = false
    var intentos:Int = 0
    var intentos_usuario:MutableList<String> = mutableListOf()

    fun imprimirProblema(n:Int){
        println("$box Problema ${n}: ${this.titulo} ")
        println()
        println( this.enunciado)
        println()
        println(" Ejemplo:")
        println("     Input: ${this.input_public} ")
        println("     Output: ${this.output_public} ")
        println()
        println(" Tu input: ${this.input_privado} $reset")
        println()
    }

    fun resolverProblema(){
        var seguir = true
        do{
            this.intentos++
            println("$box ${this.enunciado}")
            println()
            println(" Ejemplo: ")
            println("     Input: ${this.input_public} ")
            println("     Output: ${this.output_public} ")
            println()
            println(" Tu input: ${this.input_privado} $reset")
            println()
            print("$mgreen Escribe aquí tu output:   $reset")
            val respuestausuario = scanner.next()
            if(respuestausuario == this.output_privado) {
                println()
                println("$box$green Es correcto $reset")
                this.resuelto = true
            }
            else println("${box}${red} Es incorrecto $reset")
            this.intentos_usuario.add(respuestausuario)
            Database().actualizarProblema(this)
        }while (!this.resuelto)
    }

    fun imprimirEstat(){
        println()
        println("$box Problema: ${this.titulo} ")
        println(" Cantidad de intentos: ${this.intentos} ")
        println(" Tus intentos: ")
        for(i in this.intentos_usuario.indices) println("   - ${this.intentos_usuario[i]} ")
        print(" Estado:  ")
        if(!this.resuelto) print("no ")
        println("resuelto$reset")
        println()
    }
}