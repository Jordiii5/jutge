import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.io.path.Path
import kotlin.io.path.readLines

class Problemas {
    var problemas = mutableListOf<Problema>()

    init{
        problemas = Database().recibirProblemas()
    }

    fun resueltos(no:Boolean):List<Problema>{
        val resueltos = mutableListOf<Problema>()
        val noResueltos = mutableListOf<Problema>()
        for(problema in this.problemas){
            if(problema.resuelto) resueltos.add(problema)
            else noResueltos.add(problema)
        }
        return if(no) noResueltos
        else resueltos
    }

    fun ordenarPor(tema:String):List<Problema>{
        val ordenados = mutableListOf<Problema>()
        for(problema in this.problemas){
            if(problema.tema == tema) ordenados.add(problema)
        }
        return ordenados
    }
}