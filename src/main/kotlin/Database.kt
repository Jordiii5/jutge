import java.sql.SQLException

class Database {
    fun anadirProblema(problema: Problema){
        val query = conexion!!.prepareStatement("INSERT INTO problemas (tema, titulo, enunciado, input_public, output_public, input_privado, output_privado, resuelto, intentos)" +
                "VALUES (?,?,?,?,?,?,?,?,?)")
        try {
            query.setString(1, problema.tema)
            query.setString(2, problema.titulo)
            query.setString(3, problema.enunciado)
            query.setString(4, problema.input_public)
            query.setString(5, problema.output_public)
            query.setString(6, problema.input_privado)
            query.setString(7, problema.output_privado)
            query.setBoolean(8, problema.resuelto)
            query.setInt(9, problema.intentos)
            query.executeUpdate()
            query.close()
        }catch (e: SQLException) {
            println("Error ${e.errorCode} al añadir el problema: ${e.message}")
        }
    }

    fun recibirProblemas():MutableList<Problema>{
        val problemas = mutableListOf<Problema>()
        val query = conexion!!.createStatement().executeQuery("SELECT * FROM problemas")

        try {
            while(query.next()){
                val id_problema = query.getInt("id_problema")
                val tema = query.getString("tema")
                val titulo = query.getString("titulo")
                val enunciado = query.getString("enunciado")
                val input_public = query.getString("input_public")
                val output_public = query.getString("output_public")
                val input_privado = query.getString("input_privado")
                val output_privado= query.getString("output_privado")
                val problema = Problema(
                    id_problema,
                    tema,
                    titulo,
                    enunciado,
                    input_public,
                    output_public,
                    input_privado,
                    output_privado
                )
                problema.intentos_usuario= this.recibirIntentos(problema.id_problema)
                problemas.add(problema)
            }
            query.close()
        }catch (e: SQLException) {
            println("Error ${e.errorCode} al recibir los problemas: ${e.message}")
        }

        return problemas
    }

    fun anadirIntento(idProblema:Int,intento:String){
        try {
            conexion!!.createStatement().executeUpdate("INSERT INTO intentos_usuario (id,intento) VALUES ($idProblema,$intento)")
        }catch (e: SQLException) {
            println("Error ${e.errorCode} al añadir el intento: ${e.message}")
        }
    }

    fun recibirIntentos(idProblema: Int):MutableList<String>{
        val intents = mutableListOf<String>()
        val query = conexion!!.createStatement().executeQuery("SELECT * FROM intentos_usuario WHERE id_problema = $idProblema")
        try{
            while(query.next()){
                val intento = query.getString("intento")
                intents.add(intento)
            }
            query.close()
        }catch (e: SQLException) {
            println("Error ${e.errorCode} al recibir los intentos: ${e.message}")
        }
        return intents
    }

    fun actualizarProblema(problema: Problema){
        val update = "UPDATE problemes SET tema = ?, titulo = ?, enunciado = ?,input_public = ?, output_public = ?,input_privado = ?,output_privado = ?,resuelto = ?,intentos = ? WHERE id = ?"
        val query = conexion!!.prepareStatement(update)
        try{
            query.setString(1, problema.tema)
            query.setString(2, problema.titulo)
            query.setString(3, problema.enunciado)
            query.setString(4, problema.input_public)
            query.setString(5, problema.output_public)
            query.setString(6, problema.input_privado)
            query.setString(7, problema.output_privado)
            query.setBoolean(8, problema.resuelto)
            query.setInt(9, problema.intentos)
            query.setInt(10, problema.id_problema)
            query.executeUpdate()
            query.close()
        }catch (e: SQLException) {
            println("Error ${e.errorCode} al actualizar el problema: ${e.message}")
        }
    }
}